// console.log("Hello World!");
/*
function sumNumbers(){
	let sumNum1 = prompt("Enter 1st Number?");
	let sumNum2 = prompt("Enter 2nd Number?");
	sumEqual = (sumNum1 + sumNum2);
	console.log("Displayed sum of " + sumNum1 +" and " + sumNum2);
	console.log(sumEqual);
}
sumNumbers();
*/

//Functions
// Parameters and Arguments

/*
function printInfo(){
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}
printInfo();
*/

// Function has parameter and argument
function printName(firstname) { //name --> parameter
    console.log("My name is " + firstname);
}

printName("Juana"); //Juana --> argument
printName("James");
printName("Cameron");

// 1. printName("Juana"); >> ARGUMENT
// 2. will be stored to our PARAMETER >> function printName(name)
// 3. The information/data stored in a Parameter can be used to the code block inside a function.

// Now we have a reusable function / reusable task but could have different output based
// what value to process, with the help of..
// [SECTION] Parameters and Arguments
	// Parameter
		// "firstName" is called a parameter
		// A "parameter" acts as named variable/ container that exists only inside a function
		// It is osed to store information that is provided to a function when it is called/invoke

	// Argument
	// "Juana", "John" ,and "Cena" the information/data provided directly into thr function is called argument
	// values passed when invoking a function are called arguments
	// these arguments are then stored as the parameter within the function


let sampleVariable = "Inday";

printName(sampleVariable);

//Function arguments cannot be used by a function if there are no parameters provided within the function.

function checkDivisibilityBy8(num) {
    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

//You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

// Function as Arguments

// Function parameters can also accept other functions as arguments
// Some complex functions use other functions as arguments to perform more complicated results
// This will be further seen when we discuss array methods.

function argumentFunction() {
    console.log("This function was passed as an argument before the message.")
}

function invokeFunction(argumentFunction) {
    argumentFunction();
}

// Adding and removing the parentheses "()" impacts the output of JavaScript heavily
// When a function is used with parentheses "()", it denotes invoking/calling a function
// A function used without a parenthesis is normally associated with using the function as an argument to another function

invokeFunction(argumentFunction);
//or finding more information about a function in the console using console.log()
console.log(argumentFunction);

// Multiple Parameters

function createFullName(firstName, middleName, lastName) {
    console.log("My full name is " +firstName+ " " +middleName+ " " + lastName);
}

createFullName("Alden", "Palacios", "Ulep");
createFullName("", "Ulep ", " Alden");

function getDifferenceOf8Minus4(numA, numB){
console.log("Difference: " + (numA - numB));

}
getDifferenceOf8Minus4(8,4);
getDifferenceOf8Minus4(4, 8);
// ThiSs will result to logical erro

// Multiple parameter using stored data in a variable
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

//Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

// [SECTION] Return Statement
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName) {
    console.log("Test Console");
    return firstName + ' ' + middleName + ' ' + lastName;
    console.log("This will not be listed in the console");
}

let completeName = returnFullName("Juan", "Dela", "Cruz");
console.log(completeName);

console.log("I am "+completeName);

// In our example, the "returnFullName" function was invoked/called in the same line as declaring a variable.

// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable

console.log(returnFullName(firstName, middleName, lastName));


//On the other hand, when a function that only has console.log() to display its result it will return undefined instead.

function printPlayerInfo(username, level, job) {
    console.log("Username: " + username);
    console.log("Level: " + level);
    console.log("Job: " + job);

    return "Username " + username;
}

let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
console.log(user1);







